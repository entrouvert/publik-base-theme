#!/bin/bash

# add a trap to ensure our livereload
# is stopped properly on ctrl-c
trap 'kill %1' SIGINT

./livereload-server & iwatch -r -t '.scss' -e modify -c "python3 setup.py compile_scss" .
