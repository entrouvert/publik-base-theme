{% extends "emails/body_base.txt" %}{% block content %}{% autoescape off %}Bonjour {{ full_name }},

Votre compte sur le site {{site}} a été supprimé, conformément à votre demande.

Veuillez noter que vous ne pourrez plus vous connecter à votre compte à
partir de maintenant.
{% endautoescape %}{% endblock %}
