{% extends "emails/body_base.txt" %}
{% block content %}{% autoescape off %}Bonjour,

Vous avez demandé à changer l’adresse électronique de votre compte
de {{ old_email }} à {{ email }}.

Pour valider ce changement, veuillez cliquer sur le lien suivant :

  {{ link }}

Attention, sans validation de votre part dans les {{token_lifetime}},
votre demande sera annulée.

{% endautoescape %}{% endblock %}
