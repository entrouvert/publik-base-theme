{% extends "emails/body_base.txt" %}{% block content %}{% autoescape off %}Bonjour {{ user.get_full_name }},

Votre compte est inactif et va bientôt être supprimé.

Pour conserver votre compte, il vous suffit de vous connecter
avant {{ days_to_deletion }} jours.

Me connecter : {{ login_url }}

Dans le cas contraire, il sera supprimé passé ce délai.
{% endautoescape %}{% endblock %}
