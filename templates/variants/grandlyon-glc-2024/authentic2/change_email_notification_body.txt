{% extends "emails/body_base.txt" %}
{% block content %}Bonjour,

Vous avez demandé à changer l'adresse de courriel de votre compte
de {{ old_email }} à {{ email }}.

Pour valider ce changement, veuillez cliquer sur le lien suivant :

  {{ link }}

Ce courriel sera valide pendant {{ token_lifetime }}.
{% endblock %}
