Bonjour,

Vous avez demandé à réinitialiser votre mot de passe du compte usager de
Montpellier Méditerranée Métropole.

Voici pour rappel votre identifiant : {% firstof user.username user.email %}

Pour réinitialiser votre mot de passe, cliquez sur le lien suivant :

{{ reset_url }}

Nous vous remercions d’utiliser notre portail de e-services.

Les équipes de Montpellier Méditerranée Métropole.

--------------------------------------------------
Important : Cet email est généré par un automate, merci de ne pas y répondre.
