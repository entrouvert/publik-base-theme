#! /usr/bin/env python3

import subprocess
import sys
from urllib.parse import urljoin

from lxml import etree
from lxml.html import HTMLParser

html_parser = HTMLParser(encoding='utf-8')

# platform to synchronize should be specified in parameter
if len(sys.argv) > 1:
    PLATFORM_BASE_URL = sys.argv[1]
else:
    PLATFORM_BASE_URL = 'https://test.villeurbanne.fr/'

HEAD_URL = urljoin(PLATFORM_BASE_URL, 'publik/get/head')
HEADER_URL = urljoin(PLATFORM_BASE_URL, 'publik/get/header')
FOOTER_URL = urljoin(PLATFORM_BASE_URL, 'publik/get/footer')
BANNER_URL = urljoin(PLATFORM_BASE_URL, 'publik/get/banner')

JS_URL = urljoin(PLATFORM_BASE_URL, 'build/publik/publik-app.js')

HEAD_FILENAME = 'head.html'
HEADER_FILENAME = 'header.html'
FOOTER_FILENAME = 'footer.html'
BANNER_FILENAME = 'banner.html'

subprocess.call(['wget', '--quiet', '-O', HEAD_FILENAME, '--convert-links', HEAD_URL])

head_content = open(HEAD_FILENAME).read().strip()
head_content = head_content.replace(HEAD_FILENAME, '')

root = etree.fromstring(head_content, parser=html_parser)

# remove external undesired elements
elements = ['font-awesome']
for link in root.xpath('//link'):
    for element in elements:
        if element in link.attrib['href']:
            parent = link.getparent()
            parent.remove(link)

external_js = '<script type="text/javascript" src="%s"></script>' % JS_URL
head_content = etree.tostring(root, method='html').decode()

# remove closing <html> because it will be closed later
head_content = head_content.replace('</html>', '')
head_content = head_content.replace(
    '</head>',
    external_js
    + '''
{% block head %}{% endblock %}
<title>{% block global_title %}{% endblock %}</title>
</head>''',
)

subprocess.call(['wget', '--quiet', '-O', HEADER_FILENAME, '--convert-links', HEADER_URL])
header_content = open(HEADER_FILENAME).read().strip()
header_content = header_content.replace(HEADER_FILENAME, '')

root = etree.fromstring(header_content, parser=html_parser)
for link in root.xpath('//li[@class="compte"]'):
    for child in link:
        link.remove(child)
    link.text = '{% block user-info %}{% endblock %}'

header_content = etree.tostring(root, method='html').decode()

subprocess.call(['wget', '--quiet', '-O', BANNER_FILENAME, '--convert-links', BANNER_URL])
banner_content = open(BANNER_FILENAME).read().strip()
banner_content = banner_content.replace(BANNER_FILENAME, '')

publik_content = '''<div class="publik-container">
{% block nav %}{% endblock %}
{% block site-content %}
{% block messages %}
{% endblock %}
{% endblock %}
</div>'''

subprocess.call(['wget', '--quiet', '-O', FOOTER_FILENAME, '--convert-links', FOOTER_URL])
footer_content = open(FOOTER_FILENAME).read().strip()
footer_content = footer_content.replace(FOOTER_FILENAME, '')
publik_footer = '''<div id="footer-wrapper">
<div id="footer">
{% block footer %}
{% endblock %}
</div>
</div>'''

content = '\n'.join(
    [
        head_content,
        '<body {% block bodyargs %}{% endblock %}>',
        header_content,
        banner_content,
        publik_content,
        publik_footer,
        footer_content,
        '{% block tracking %}{% endblock %}',
        '</body>',
        '</html>',
    ]
)
open('base-theme.html', 'w').write(content)
