# publik-base-theme
# Copyright (C) 2014-2024 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.commands import makemessages


class Command(makemessages.Command):
    def handle(self, *args, **options):
        if not options.get('add_location') and self.gettext_version >= (0, 19):
            options['add_location'] = 'file'
        return super().handle(*args, **options)

    def find_files(self, root):
        for file in super().find_files('templates'):
            file.locale_dir = 'publik_base_theme/locale'
            yield file
