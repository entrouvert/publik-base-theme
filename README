Base theme for Publik
=====================

Variables
---------

 - css_variant: this is the main variable, it is used to build the path to the
   CSS file. (/static/{{css_variant}}/style.css)

 - favicon: path to favicon file, relative to /static/ directory.

 - logo_link_url: link to be used for the top logo, defaults to the site root.


Code Style
----------

black is used to format the Python code, using these parameters:

    black --target-version py37 --skip-string-normalization --line-length 110

Similarly, isort is used for imports, using these parameters:

    isort --profile black --line-length 110

djhtml is used to automatically indent html files, using those parameters:

    djhtml --tabwidth 2

There is .pre-commit-config.yaml to use pre-commit to automatically run black,
isort and djhtml before commits. (execute `pre-commit install` to install the
git hook.)


License
-------

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <http://www.gnu.org/licenses/>.

This program incorporates independent elements, with their own authors and
copyright notices:

* Yanone Kaffeesatz font
  # https://www.fontsquirrel.com/fonts/yanone-kaffeesatz
  #
  # Licensed under the SIL Open Font License, Version 1.1.
  # https://www.fontsquirrel.com/license/yanone-kaffeesatz

* Ubuntu font
  # https://www.fontsquirrel.com/fonts/ubuntu
  #
  # Licensed under the Ubuntu Font License, Version 1.0.
  # https://www.fontsquirrel.com/license/ubuntu

* D-Din font
  # https://www.fontsquirrel.com/fonts/d-din
  #
  # Licensed under the SIL Open Font License v1.10
  # https://www.fontsquirrel.com/license/d-din

* Source Sans Pro
  # https://www.fontsquirrel.com/fonts/source-sans-pro
  #
  # Licensed under the SIL Open Font License v1.10
  # https://www.fontsquirrel.com/license/source-sans-pro

* Asap fonts
  # Copyright (c) 2019, Omnibus-Type (www.omnibus-type.com | omnibus.type@gmail.com).
  # https://github.com/Omnibus-Type/Asap/
  #
  # Licensed under the SIL Open Font License v1.10
  # https://github.com/Omnibus-Type/Asap/blob/master/LICENSE.md

* Raleway font
  # Copyright (c) 2010, Matt McInerney (matt@pixelspread.com),
  # Copyright (c) 2011, Pablo Impallari (www.impallari.com|impallari@gmail.com),
  # Copyright (c) 2011, Rodrigo Fuenzalida (www.rfuenzalida.com|hello@rfuenzalida.com),
  # with Reserved Font Name Raleway
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/raleway

* Oswald font
  # Copyright (c) 2012, Vernon Adams (vern@newtypography.co.uk),
  # with Reserved Font Name Oswald
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/oswald

* Bitter font
  # Copyright (c) 2011, Sol Matas (www.huertatipografica.com.ar),
  # with Reserved Font Name “Bitter”
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/bitter

* Cantarell font
  # Copyright © 2009-2011, Understanding Limited <dave@understandinglimited.com>
  # Copyright © 2010-2011, Jakub Steiner <jimmac@gmail.com>
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://gitlab.gnome.org/GNOME/cantarell-fonts/blob/master/COPYING

* Barlow font
  # Copyright 2017 The Barlow Project Authors (https://github.com/jpt/barlow)
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/barlow

* Titillium font
  # Copyright (c) 2008-2010, Accademia di Belle Arti di Urbino
  # (www.campivisivi.net|direzione@accademiadiurbino.it),
  # with Reserved Font Name Titillium.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/Titillium

* Montserrat & Montserrat Alternates fonts
  # Copyright 2011 The Montserrat Project Authors
  # (https://github.com/JulietaUla/Montserrat)
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/montserrat

* Khand font
  # Copyright (c) 2014, Indian Type Foundry (info@indiantypefoundry.com).
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/khand

* Archivo Black
  # Copyright (c) 2012-2015, Omnibus-Type (www.omnibus-type.com|omnibus.type@gmail.com)
  # with Reserved Font Name "Archivo Black";
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/archivo-black

* Archivo Narrow
  # Copyright (c) 2012, Omnibus-Type (omnibus.type@gmail.com),
  # with Reserved Font Name "Archivo";
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/archivo-narrow

* Roboto Slab
  # Font data copyright Google 2013
  #
  # Apache License
  #
  # https://www.fontsquirrel.com/license/roboto-slab

* Lato font
  # Copyright (c) 2014, Typoland
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # http://www.latofonts.com/

* Cabin
  # Copyright (c) 2011, Pablo Impallari (www.impallari.com|impallari@gmail.com),
  # Copyright (c) 2011, Igino Marini. (www.ikern.com|mail@iginomarini.com),
  # with Reserved Font Name Cabin.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1
  #
  # https://www.fontsquirrel.com/license/cabin

* Poppins
  # Copyright (c) 2014, Indian Type Foundry (info@indiantypefoundry.com).
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1
  #
  # https://www.fontsquirrel.com/license/poppins

* Now Alt
  # Copyright (c) 2015, Alfredo Marco Pradil
  # (<http://behance.net/pradil | ammpradil@gmail.com>),
  # with Reserved Font Name Now Alt.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1

* Abel
  # Copyright (c) 2011, Matthew Desmond (http://www.madtype.com | mattdesmond@gmail.com),
  # with Reserved Font Name Abel.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/abel

* Work Sans
  # Copyright (c) 2014-2015 Wei Huang (wweeiihhuuaanngg@gmail.com)
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/work-sans

* Exo 2
  # Copyright (c) 2013, Natanael Gama (www.ndiscovered.com . info(at)ndiscovered.com), with Reserved Font Name Exo.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/exo-2

* Droid Sans
  # Copyright © 2006, 2007, 2008, 2009, 2010 Google Corp.
  # Droid is a trademark of Google Corp.
  #
  # Licensed under the Apache License, Version 2.0 (the "License");
  # you may not use this file except in compliance with the License.
  # You may obtain a copy of the License at
  #
  # http://www.apache.org/licenses/LICENSE-2.0

* Carme
  # Copyright (c) 2010-2011, Rubén Prol (ipanemagrafica@gmail.com|www.ipanemagrafica.com), with Reserved Font Name Carme.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # This license is available with a FAQ at: http://scripts.sil.org/OFL

* PT Sans
  # Copyright © 2009 ParaType Ltd.
  # with Reserved Names "PT Sans" and "ParaType".
  #
  # Paratype PT Sans Free Font License v1.00.
  #
  # https://www.fontsquirrel.com/license/pt-sans

* Catamaran
  # Copyright 2014 Pria Ravichandran (pria.ravichandran@gmail.com)
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/catamaran

* Quattrocento Sans
  # Copyright (c) 2011, Pablo Impallari (www.impallari.com|impallari@gmail.com),
  # Copyright (c) 2011, Igino Marini. (www.ikern.com|mail@iginomarini.com),
  # with Reserved Font Name Quattrocento Sans.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/quattrocento-sans

* Muli
  # Copyright (c) 2011 by vernon adams (vern@newtypography.co.uk),
  # with Reserved Font Name “Muli”.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/muli

* Open Sans Condensed
  # Copyright Ascender Corp.
  #
  # Apache License
  #
  # https://www.fontsquirrel.com/license/open-sans-condensed

* Noto Sans
  # Copyright 2012 Google Inc. All Rights Reserved.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/noto-sans

* Abril Fatface
  # Copyright (c) 2011, Copyright (c) 2011, TypeTogether
  # (www.type-together.com info@type-together.com),
  # with Reserved Font Names “Abril” and “Abril Fatface”
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/abril-fatface

* Yantramanav
  # Copyright 2014, Erin McLaughlin (hello@erinmclaughlin.com).
  # Copyright 2010, Google Inc. All Rights Reserved.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/yantramanav

* Fira Sans
  # Copyright (c) 2014, Mozilla Foundation https://mozilla.org/
  # with Reserved Font Name Fira Sans.
  # Copyright (c) 2014, Telefonica S.A.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/fira-sans

* Josefin Sans
  # Copyright (c) 2010 by Typemade (hi@typemade.mx). All rights reserved.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/josefin-sans

* Dancing Script
  # Copyright 2016 The Dancing Script Project Authors (impallari@gmail.com)
  # with Reserved Font Name 'Dancing Script’.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://github.com/impallari/DancingScript

* Baloo 2
  # Baloo 2 is Copyright (c) 2019 Ek Type (www.ektype.in)
  # Licensed under the SIL Open Font License 1.1 (http://scripts.sil.org/OFL).
  #
  # https://github.com/EkType/Baloo2

* Krub
  # Copyright 2018 The Krub Project Authors (https://github.com/cadsondemak/Krub)
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://github.com/cadsondemak/Krub/

* Orkney
  # Concept Sketches: Samuel Oakes, font development: Alfredo Marco Pradil
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://fontlibrary.org/en/font/orkney

* Marianne
  # To be licensed under the MIT license - http://opensource.org/licenses/MIT
  #
  # « L'équipe DSFR m'a fait un retour : la Marianne sera a priori aussi sous
  # licence MIT. le sujet sera discuté bientôt. Dans tous les cas, la volonté est
  # d'inscrire la police dans une logique de licence libre. »
  # (November 18th 2021 - https://dev.entrouvert.org/issues/43470#note-32)
  #
  # TODO: update once the DSFR team gets back to it.

* Spectral
  # Copyright 2017 Production Type (info@productiontype.com)
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/spectral

* Dosis
  # Copyright (c) 2011, Edgar Tolentino and Pablo Impallari
  # (www.impallari.com|impallari@gmail.com),
  # Copyright (c) 2011, Igino Marini. (www.ikern.com|mail@iginomarini.com),
  # with Reserved Font Names "Dosis".
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/dosis

* Playfair Display
  # Copyright (c) 2010-2012 by Claus Eggers Sørensen (es@forthehearts.net),
  # with Reserved Font Name ‘Playfair’
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/playfair-display

* Inter
  # Copyright (c) 2016-2020 The Inter Project Authors.
  # "Inter" is trademark of Rasmus Andersson.
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://github.com/rsms/inter

* IBM Plex Serif
  # Copyright © 2017 IBM Corp. with Reserved Font Name "Plex".
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/fonts/ibm-plex

* RemixIcon
  # Copyright (c) 2020 RemixIcon.com
  #
  # Released under the Apache License Version 2.0
  #
  # https://github.com/Remix-Design/RemixIcon

* Rubik
  # Copyright (c) 2015 by Hubert & Fischer. All rights reserved
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/rubik

* Viga
  # Copyright (c) 2011 Fontstage (info@fontstage.com), with Reserved Font Name “Viga”
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/viga

* Manrope
  # Copyright 2018 The Manrope Project Authors. All rights reserved
  #
  # TThis Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://github.com/sharanda/manrope

* Butler Stencil
  # Copyright (c) 2019 Fabian De Smet, with Reserved Font Name "Butler Stencil"
  #
  # This Font Software is licensed under the Creative Commons Attribution-ShareAlike License, version 4.00
  #
  # https://www.fontsquirrel.com/license/butler

* Public Sans
  # Copyright (c) 2015 Impallari Type (www.impallari.com), with Reserved Font Name “Public Sans”
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/public-sans

* MuseoModerno
  # Copyright (c) 2020, Omnibus-Type (www.omnibus-type.com|omnibus.type@gmail.com).
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://github.com/Omnibus-Type/MuseoModerno/blob/master/LICENSE.md

* Nunito Sans
  # Copyright 2016 The Nunito Project Authors (contact@sansoxygen.com),
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/nunito-sans

* Arimo
  # Copyright (c) 2013 Steve Matteson.
  #
  # This Font Software is licensed under the Apache License Version 2.0.
  #
  # https://www.fontsquirrel.com/license/arimo

* Kumbh Sans
  # Copyright 2020 The KumbhSans Project Authors (https://github.com/xconsau/KumbhSans)
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://github.com/xconsau/KumbhSans/blob/master/OFL.txt

* Kufam
  # Copyright 2019 The Kufam Project Authors (https://github.com/originaltype/kufam)
  #
  # Licensed under the SIL Open Font License 1.1 (http://scripts.sil.org/OFL).
  #
  # https://font.download/font/kufam-2

* Signika
  # Copyright (c) 2011 by Anna Giedryś (http://ancymonic.com),
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/signika

* Be Vietnam Pro
  # Copyright 2021 The Be Vietnam Pro Project Authors (https://github.com/bettergui/BeVietnamPro)
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://raw.githubusercontent.com/bettergui/BeVietnamPro/main/OFL.txt

* Nunito
  # Copyright 2014 The Nunito Project Authors (https://github.com/googlefonts/nunito)
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://github.com/googlefonts/nunito/blob/main/OFL.txt

* Syne
  # Copyright (c) 2017 by Bonjour Monde (http://bonjourmonde.net),
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://www.fontsquirrel.com/license/syne

* Urbanist
  # Copyright (c) 2024, Corey Hu (corey@coreyhu.com) (https://github.com/coreyhu/Urbanist)
  #
  # This Font Software is licensed under the SIL Open Font License, Version 1.1.
  #
  # https://openfontlicense.org/

* Luciole
  # Luciole © Laurent Bourcellier & Jonathan Perez (https://www.luciole-vision.com)
  #
  # These fonts are freely available under Creative Commons Attribution 4.0 International Public License.
  #
  # https://www.luciole-vision.com/Fichiers/Read%20Me.txt

~~~~

Some theme variant files have their own specific licenses; refer to
static/$theme/README for details.
