#! /usr/bin/env python3
# inkscape wrapper to support command-line parameters for <1.0 and 1.0
# versions, and filter output to avoid log spamming with dbus error
# (https://gitlab.com/inkscape/inkscape/-/issues/294)

import subprocess
import sys

inkscape_version = subprocess.check_output('inkscape --version', shell=True)
args = sys.argv[1:]
if b'Inkscape 0' not in inkscape_version:
    # --export-png replaced by --export-filename
    # --without-gui and --file removed
    args = [
        x.replace('--export-png', '--export-filename') for x in args if x not in ('--without-gui', '--file')
    ]

inkscape = subprocess.Popen(
    ['inkscape'] + args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True
)

for line in inkscape.stdout:
    if 'dbus_' in line and ": assertion 'connection != NULL' failed" in line:
        continue

    sys.stdout.write(line)

sys.exit(inkscape.returncode)
