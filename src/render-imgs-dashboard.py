#! /usr/bin/env python3

# publik-base-theme
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import re
import subprocess
import sys
import tempfile
import xml.etree.ElementTree as ET

base_dir = os.path.dirname(__file__)
inkscape = os.path.abspath(os.path.join(os.path.dirname(__file__), 'inkscape_wrapper.py'))

names = {
    '12': 'profile',
    '13': 'documents',
    '14': 'family',
    '15': 'requests',
    '16': 'links',
    '17': 'billings',
}

filenames = [
    'EO_CONNECTVILLE_PUBLIK_PICTO_12.svg',
    'EO_CONNECTVILLE_PUBLIK_PICTO_13.svg',
    'EO_CONNECTVILLE_PUBLIK_PICTO_14.svg',
    'EO_CONNECTVILLE_PUBLIK_PICTO_15.svg',
    'EO_CONNECTVILLE_PUBLIK_PICTO_16.svg',
    'EO_CONNECTVILLE_PUBLIK_PICTO_17.svg',
]

parser = argparse.ArgumentParser()
parser.add_argument('path', help='out path')
parser.add_argument('--normal', default='4d4d4d')
parser.add_argument('--normal-height')
parser.add_argument('--normal-width')
parser.add_argument('--selected')
parser.add_argument('--selected-height')
parser.add_argument('--selected-width')
parser.add_argument('--title')
parser.add_argument('--title-height')
parser.add_argument('--title-width')
parser.add_argument('--skip-existing', action='store_true')

args = parser.parse_args()
path_out = args.path

variants = {}
for variant in ('normal', 'selected', 'title'):
    if not getattr(args, variant):
        continue
    variant_code = variant
    if variant_code == 'normal':
        variant_code = ''
    variants[variant_code] = {
        'colour': getattr(args, variant),
        'height': getattr(args, variant + '_height'),
        'width': getattr(args, variant + '_width'),
    }

os.makedirs(path_out, exist_ok=True)

for filename in filenames:
    icon_name = names.get(re.search(r'\d+', filename).group())

    for variant_name, variant_data in variants.items():
        variant_colour = variant_data.get('colour')
        variant_height = variant_data.get('height')
        variant_width = variant_data.get('width')
        tree = ET.parse(open(os.path.join(base_dir, 'pictos', filename)))
        if variant_name:
            out_filepath = os.path.join(path_out, '%s-%s.png' % (icon_name, variant_name))
        else:
            out_filepath = os.path.join(path_out, '%s.png' % icon_name)

        if args.skip_existing and os.path.exists(out_filepath):
            continue

        for node in tree.iter():
            tag_name = node.tag.split('}')[-1]
            if tag_name == 'path':
                node.attrib['fill'] = '#%s' % variant_colour
                node.attrib['stroke'] = 'none'
            if tag_name in ('circle', 'rect'):
                node.attrib['fill'] = 'none'
                node.attrib['stroke'] = '#%s' % variant_colour

        with tempfile.NamedTemporaryFile(suffix='.svg') as tmpfile:
            tree.write(tmpfile.name)
            tmpfile.flush()
            cmd = [
                inkscape,
                '--without-gui',
                '--file',
                tmpfile.name,
                '--export-area-drawing',
                '--export-png',
                out_filepath,
            ]
            if variant_width:
                cmd.extend(['--export-width', variant_width])
            else:
                cmd.extend(['--export-height', variant_height or '60'])
            subprocess.call(cmd)
