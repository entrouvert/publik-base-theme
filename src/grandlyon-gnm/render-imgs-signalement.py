#! /usr/bin/env python

import argparse
import os
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument('path', help='out path')
parser.add_argument('--color', default='2A2E49')

args = parser.parse_args()

for filename in os.listdir(os.path.join(args.path, 'src')):
    outname = os.path.basename(filename)
    fd = open('/tmp/tmp-%s' % outname, 'w')
    fd.write(
        open(os.path.join(args.path, 'src', filename)).read().replace('fill:#DA0000', 'fill:#%s' % args.color)
    )
    fd.close()
    subprocess.call(
        [
            'inkscape',
            '--without-gui',
            '--file',
            '/tmp/tmp-%s' % outname,
            '--export-area-drawing',
            '--export-png',
            os.path.join(args.path, outname.replace('.svg', '-%s.png' % args.color)),
            '--export-height',
            '31',
        ]
    )
