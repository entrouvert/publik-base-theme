$(function() {
  var mql = window.matchMedia("screen and (max-width: 800px)");
  if (mql.matches) {
    /* move form title on mobile */
    $('body.has-picture h1.form-content--title').attr('id', 'mobile-form-title-h1').detach().prependTo('#columns');
    $('body.has-picture div#rub_service > h2').attr('id', 'mobile-form-title-h2').detach().prependTo('#columns');
    var $mobile_form_title = $('#mobile-form-title-h1, #mobile-form-title-h2');
    $mobile_form_title.removeClass('form-content--title');
    if ($mobile_form_title.height() > 150) {
      $mobile_form_title.addClass('long-title');
    }
  }
});
