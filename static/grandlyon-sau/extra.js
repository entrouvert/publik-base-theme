$(function() {
    if (document.body.attributes['data-picture']) {
	var sheet = document.head.appendChild(document.createElement("style")).sheet;
	sheet.insertRule('#content div.bandeau div {background-image: url("' + document.body.attributes['data-picture'].value + '"); background-repeat: no-repeat; background-size: calc(100% - 4em); height: 350px; background-position: center bottom', 0);
    }
});
