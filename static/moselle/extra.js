$(function() {
  /* set page picture as background */
  if (document.body.attributes['data-picture']) {
    var sheet = document.head.appendChild(document.createElement("style")).sheet;
    sheet.insertRule('body.has-picture nav::after { background-image: url(' + document.body.attributes['data-picture'].value + ')', 0);
  }
  /* add a class to body when the page got scrolled */
  $(window).on('scroll', function() {
    if ($(window).scrollTop() == 0) {
      $('body').removeClass('scrolled');
    } else {
      $('body').addClass('scrolled');
    }
  });
});
