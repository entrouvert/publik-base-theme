$(function() {
  var mql = window.matchMedia("screen and (max-width: 800px)");
  $('div.cell.map').on('combo:map-feature-prepare', function(ev, info) {
    info.layer.on('click', function(ev) {
      $('.parking').removeClass('shown');
      $('#parking-' + info.layer.feature.properties.Ordre).addClass('shown');
      if (!mql.matches) {
        window.location.hash = "parking-" + info.layer.feature.properties.Ordre;
      }
    });
  });
  $('div.cell').delegate('.parking', 'click', function(e) {
    $(this).removeClass('shown');
  });
  if (mql.matches) {
    /* move footer to navigation menu on mobile... */
    var $burger = $('#nav ul');
    $('footer .menucell li').each(function(idx, li) {
      $(li).detach().addClass('footer-link').appendTo($burger);
    });
    /* and compute map height to match screen height */
    if ($('.cell.parkings').length) {
      var cell_top = $('div.cell.map div.combo-cell-map.leaflet-container').position().top;
      $('div.cell.map div.combo-cell-map.leaflet-container').css('height',
              'calc(100vh - ' + (64 + cell_top) + 'px)');
    }
  }
});
