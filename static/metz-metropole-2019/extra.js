// from django/contrib/admin/static/admin/js/urlify.js
var LATIN_MAP = {
    'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE',
    'Ç': 'C', 'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I',
    'Î': 'I', 'Ï': 'I', 'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O',
    'Õ': 'O', 'Ö': 'O', 'Ő': 'O', 'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U',
    'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH', 'Ÿ': 'Y', 'ß': 'ss', 'à': 'a',
    'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
    'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i',
    'ï': 'i', 'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o',
    'ö': 'o', 'ő': 'o', 'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u',
    'ű': 'u', 'ý': 'y', 'þ': 'th', 'ÿ': 'y'
};

function downcode(string) {
  return string.toLowerCase().replace(/[^A-Za-z0-9\[\] ]/g,function(a){ return LATIN_MAP[a]||a })
};

$(function() {
  if (navigator.userAgent.indexOf("MSIE ") > 0 || navigator.userAgent.indexOf("Trident") > 0) {
    $('.page-signalements .gru-content a.big-image-link').css('background-position', 'bottom 40px left 50%');
    $('.page-index .gru-content a.big-image-link').css('background-position', 'top -20px left 50%');
  }

  // elus page
  $('button.metropole').on('click', function() {
    $('#columns').removeClass();
    $('#filter-view input').val('');
    var mql = window.matchMedia("screen and (max-width: 800px)");
    if (mql.matches) { /* on mobile */
      $('#filter-view').toggleClass('with-filters');
    } else {
      $('#mobile-filters').hide();
    }
    $('#columns .cell').hide();
    $('#columns .cell.metropole').show();
  });
  $('button.commune').on('click', function() {
    $('#columns').removeClass();
    $('#filter-view input').val('');
    if (mql.matches) { /* on mobile */
      $('#filter-view').toggleClass('with-filters');
    } else {
      $('#mobile-filters').hide();
    }
    $('#columns .cell').hide();
    $('#columns .cell.commune').show();
  });
  $('button.favoris').on('click', function() {
    $('#columns').removeClass().addClass('display-favoris');
    $('#filter-view input').val('');
    if (mql.matches) { /* on mobile */
      $('#filter-view').toggleClass('with-filters');
    } else {
      $('#mobile-filters').hide();
    }
    $('#columns .cell').hide();
    $('#columns .cell.favori').show();
  });
  $('button.tout').on('click', function() {
    $('#columns').removeClass();
    $('#filter-view input').val('');
    $('#columns .cell').show();
  });
  $('button.search').on('click', function() {
    $('#filter-view').removeClass('with-filters');
    $('#filter-view').toggleClass('with-search');
    $('#filter-view input').focus();
  });

  var mql = window.matchMedia("screen and (max-width: 800px)");
  if (mql.matches) { /* on mobile */
    /* split content top and move carrousel to the bottom */
    var $carrousel = $('.content-top #carrousel').detach();
    $('<div class="content-top"></div>').prependTo($('#footer'));
    $('#footer .content-top').append($carrousel);
    /* elus filter */
    var $elus_filters = $('<div id="mobile-filters"></div>');
    $('#filter-view button.desktop-only').detach().removeClass('desktop-only').appendTo($elus_filters);
    $elus_filters.appendTo($('#filter-view'));
    $('button.filtrer').on('click', function() {
      $('#filter-view').toggleClass('with-filters');
    });
  }

  $('#filter-view input').on('change keyup paste', function(e) {
    if (e.keyCode == 13) {  // enter
      $(this).blur();
      return false;
    }
    var q = downcode($(this).val());
    if (q) {
      $('#columns .cell').each(function() {
        var cell_text = downcode($(this).text());
        if (cell_text.search(q) == -1) {
          $(this).hide();
        } else {
          $(this).show();
        }
      });
    } else {
      $('#columns .link-cell').show();
    }
  });

});
