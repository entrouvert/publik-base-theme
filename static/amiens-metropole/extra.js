$(function() {
	/* set page picture as background */
	if (document.body.attributes['data-picture']) {
		var sheet = document.head.appendChild(document.createElement("style")).sheet;
		sheet.insertRule('body.has-picture #content .topbanner-wrapper { background: white 0 0 / cover no-repeat url("' + document.body.attributes['data-picture'].value + '"); }', 0);
	}

	$('#main-content-wrapper').delegate('div.wcsformsofcategorycell h2', 'click', function() {

		$(this).parent().find('ul').each(function(index, elt) {
			var $elt = $(elt);
			if (! $elt.hasClass('on')) {
				$elt.removeClass('on');
			}
			$elt.toggleClass('on');
		});

		return false;
	});
})
