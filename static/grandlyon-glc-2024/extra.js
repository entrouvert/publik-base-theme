$(function () {
  $('a.a2-authorization-explanation--show-scopes').on('click', function(event) {
    $(this).hide();
    $(this).siblings('a.a2-authorization-explanation--hide-scopes').show();
    $(this).siblings('div.a2-authorization-explanation--list-scopes').show();
  });
  $('a.a2-authorization-explanation--hide-scopes').on('click', function(event) {
    $(this).hide();
    $(this).siblings('a.a2-authorization-explanation--show-scopes').show();
    $(this).siblings('div.a2-authorization-explanation--list-scopes').hide();
  });
  var login_password_block = document.getElementById('login-page');
  if (login_password_block) {
    username_input = login_password_block.querySelector("input[name='username']");
    username_input.setAttribute('placeholder', 'ericdupont@exemple.com');
    password_input = login_password_block.querySelector("input[name='password']");
    password_input.setAttribute('placeholder', 'insérer votre mot de passe...');
  }
});
