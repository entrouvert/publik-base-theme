$(document).ready(function(){
  // affichage et masquage des liens d'évitement

  $('.nav-skip a').on('focus', function () {
    $(this).parents('.container').addClass('nav-skip-focus');
  }).on('blur', function () {
    $(this).parents('.container').removeClass('nav-skip-focus');
  });
});
