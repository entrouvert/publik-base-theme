$(function() {
  $('.page-template-roadworks-map div.cell.map').on('combo:map-feature-prepare', function(ev, info) {
    info.layer.on('click', function(ev) {
      var marker = info.layer;
      var chantier_id = marker.feature.properties.IDCHANTIER || marker.feature.properties.id;
      var $cell = $('[data-chantier-id=' + chantier_id + ']');
      if ($cell.hasClass('shown')) {
        $('.chantier').removeClass('shown');
      } else {
        $('.chantier[data-chantier-id!=' + chantier_id + ']').removeClass('shown');
        $('.chantier').show();
        $cell.addClass('shown');
      }
    });
  });
  if ($('.page-template-roadworks-map').length) {
    $(document).on('combo:cell-loaded combo:roadworks-loaded', function(ev, elem) {
      $('.chantier').off('click').on('click', function() { $(this).toggleClass('shown'); });
    });
    $(document).trigger('combo:roadworks-loaded');
  }
});
