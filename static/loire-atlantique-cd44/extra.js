$(function() {
    // désactication des champs dans le formulaire récapitulatif de la saisie
    $('div.form-validation form div.page :input').attr('readonly', 'true');

    // tous les liens dans les formulaires doivent s'ouvrir dans une nouvelle page
    $('form .comment-field a').each(function() {
        $(this).attr("target","_blank");
    });

    // modales

    $("#dp44-modal-opener-1").click(function() {
        $(".dp44-modal").dialog({
            minWidth: 450
          });
        return false;
    });

    $("#dp44-modal-opener-2").click(function() {
        $(".dp44-modal").dialog({
            minWidth: 450
          });
        return false;
    });

    $("#dp44-modal-opener-3").click(function() {
        $(".dp44-modal").dialog({
            minWidth: 450
          });
        return false;
    });

    $("#dp44-modal-opener-4").click(function() {
        $(".dp44-modal").dialog({
            minWidth: 450
          });
        return false;
    });

    $("#dp44-modal-opener-5").click(function() {
        $(".dp44-modal").dialog({
            minWidth: 450
          });
        return false;
    });

    $("#dp44-modal-opener-6").click(function() {
        $(".dp44-modal").dialog({
            minWidth: 450
          });
        return false;
    });

    $("#dp44-modal-opener-7").click(function() {
        $(".dp44-modal").dialog({
            minWidth: 450
          });
        return false;
    });

    $("#dp44-modal-opener-8").click(function() {
        $(".dp44-modal").dialog({
            minWidth: 450
          });
        return false;
    });

    $("#dp44-modal-opener-9").click(function() {
        $(".dp44-modal").dialog({
            minWidth: 450
          });
        return false;
    });

    $("#dp44-modal-opener-10").click(function() {
        $(".dp44-modal").dialog({
            minWidth: 450
          });
        return false;
    });
});
