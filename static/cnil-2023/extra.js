// Add forms counter after cell title
$(document).on('combo:cell-loaded', function(e, cell){
	const cellType = ["wcs-current-forms-cell", "wcs-current-drafts-cell"]
	if (cellType.some(className => cell.classList.contains(className))) {
		const title = cell.querySelector("h2")
		const nbForms = cell.querySelectorAll('.list-of-forms > ul > li').length
		const nbTag = document.createElement('span')
		nbTag.classList.add('cnil-forms-counter')
		nbTag.innerText = nbForms
		title.appendChild(nbTag)
	}
})