$(function() {
  var mql = window.matchMedia("screen and (max-width: 800px)");
  if (mql.matches) {
    /* move footer to navigation menu on mobile... */
    var $burger = $('#nav ul');
    $('footer .menucell li').each(function(idx, li) {
      $(li).detach().addClass('footer-link').appendTo($burger);
    });
    /* fold home categories */
    $('.wcsformsofcategorycell').addClass('foldable folded');
    $('.wcsformsofcategorycell').each(function() {
      $(document).trigger('combo:cell-loaded', $(this));
    });
  }
});
