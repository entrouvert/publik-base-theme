$(function() {
  /* handle "button that should have been links" */
  $('div.cell, div#welcome-text').delegate('button[data-href]', 'click', function(e) {
    window.location = $(this).data('href');
    return false;
  });

  /* propagate click on logout in side menu to actual logout button */
  $('button.logout').on('click', function(event) {
    $('button.nav-logout').trigger('click', event);
  });
});
