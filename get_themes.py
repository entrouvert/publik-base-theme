#! /usr/bin/env python3

import argparse
import os

parser = argparse.ArgumentParser(description='Display list of generated CSS files')
parser.add_argument('--include-map-files', dest='map_files', action='store_true', help='include .map files')

args = parser.parse_args()

for dirname in sorted(os.listdir('static')):
    config = os.path.join('static', dirname, 'config.json')
    if not os.path.exists(config):
        continue
    print('static/%s/style.css' % dirname)
    if args.map_files:
        print('static/%s/style.css.map' % dirname)
    if os.path.exists(os.path.join('static', dirname, 'backoffice.scss')):
        print('static/%s/backoffice.css' % dirname)
        if args.map_files:
            print('static/%s/backoffice.css.map' % dirname)
